# `bearlibterminal`
Haskell bindings (and a Nix overlay) for [`BearLibTerminal`](http://foo.wyrd.name/en:bearlibterminal)

## Examples

Ported from the "Hello, world" example from `BearLibTerminal`'s docs.

### `BearLibTerminal.Simple`

Coming soon

### `BearLibTerminal.C`

```haskell
import qualified BearLibTerminal.C as C

main:: IO ()
main = do
  _ <- C.open

  -- Printing text
  withCString "Hello, world!" $ \str ->
    alloca $ \dimensions ->
      C.print 1 1 str dimensions

  C.refresh

  -- Wait until user close the window
  iterateWhile (/= C.tk_close) C.read

  C.close
```
