let pkgs-fork = "NixOS";
    pkgs-ref = "20.03";
    pkgs-sha256="0182ys095dfx02vl2a20j1hz92dx3mfgz2a6fhn31bqlp1wa8hlq";
    mkPkgs = import (builtins.fetchTarball {
      url = "github.com/${pkgs-fork}/nixpkgs/archive/${pkgs-ref}.tar.gz";
      sha256 = pkgs-sha256;
    });

    bearlibterminal = import ./overlays/bearlibterminal.nix;
in
mkPkgs { overlays = [ bearlibterminal ]; }
