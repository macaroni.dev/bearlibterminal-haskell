{ stdenv, fetchFromGitHub, cmake, pkgconfig, libGLU, libX11 }:

stdenv.mkDerivation rec {
  name = "bearlibterminal";
  version = "0.15.7";

  src = fetchFromGitHub {
    owner = "cfyzium";
    repo = "bearlibterminal";
    rev = "d347d14666671f6118ede40d7b3b3ef82edbd462";
    sha256 = "02g6x3rvwczw83xzrs3j85x6n3d4765x83jn5h2f1hlpshjdz3gy";
  };

  
  nativeBuildInputs = [ cmake pkgconfig ];
  buildInputs = [ libGLU libX11 ];

  # idk if this installPhase is Best Practice heh
  installPhase = ''
    mkdir -p $out/lib
    mkdir -p $out/include

    cp ../Output/Linux64/libBearLibTerminal.so $out/lib/
    cp ../Terminal/Include/C/BearLibTerminal.h $out/include/

    # pkgconfig
    mkdir -p $out/lib/pkgconfig
    cat >> $out/lib/pkgconfig/bearlibterminal.pc << EOF

    Name: bearlibterminal
    Description: bearlibterminal
    Version: ${version}
    Libs: -L$out/lib -lBearLibTerminal
    Cflags: -I$out/include
    EOF
  '';
}
