{-# LANGUAGE StrictData #-}
{-# LANGUAGE GADTs #-}

module BearLibTerminal.Config where

import Data.List (intercalate)
import Foreign.C.String (withCString)
import qualified Foreign.Marshal.Utils as Foreign

import qualified BearLibTerminal.C as C

data Option arg where
  TerminalEncoding     :: Option String
  WindowSize           :: Option (Int, Int)
  WindowCellSize       :: Option (Maybe Int)
  WindowTitle          :: Option String
  WindowIcon           :: Option FilePath
  WindowResizeable     :: Option Bool
  WindowFullScreen     :: Option Bool
  InputFilter          :: Option () -- TODO: Type
  InputPreciseMouse    :: Option Bool
  InputMouseCursor     :: Option Char
  InputCursorBlinkRate :: Option Int
  InputAltFunctions    :: Option Bool
  OutputPostFormatting :: Option Bool
  OutputVSync          :: Option Bool
  OutputTabWidth       :: Option Int
  LogFile              :: Option FilePath
  LogLevel             :: Option LogLevel
  LogMode              :: Option LogMode

data LogLevel = DEBUG | INFO | WARN | ERROR deriving (Eq, Show)
data LogMode = APPEND | TRUNCATE deriving (Eq, Show)

data Param where
  MkParam :: Option arg -> arg -> Param

type Params = [Param]

(.=) :: Option arg -> arg -> Param
(.=) = MkParam

encode :: Param -> String
encode = undefined

encodeAll :: Params -> String
encodeAll = intercalate "; " . fmap encode

-- TODO: Font config (in Option type?)

-- TODO: Call set()

set :: Params -> IO Bool
set ps = Foreign.toBool <$> withCString (encodeAll ps) C.set

get :: Option arg -> IO arg
get = undefined

