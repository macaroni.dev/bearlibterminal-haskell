module BearLibTerminal.Playground where

import Foreign.C.String      (withCString)
import Foreign.Marshal.Alloc (alloca)
import Control.Monad.Loops   (iterateWhile)

import qualified BearLibTerminal.C as C

helloWorld :: IO ()
helloWorld = do
  _ <- C.open

  -- Printing text
  withCString "Hello, world!" $ \str ->
    alloca $ \dimensions ->
      C.print 1 1 str dimensions

  C.refresh

  -- Wait until user close the window
  iterateWhile (/= C.tkClose) C.read

  C.close
