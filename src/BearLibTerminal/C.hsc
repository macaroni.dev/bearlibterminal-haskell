{-# LANGUAGE StrictData #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}

{-# OPTIONS_GHC -funbox-strict-fields #-}

-- CPP doesn't like the single tick
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}

module BearLibTerminal.C where

import Prelude hiding (print, read)

import Data.Int
import Data.Bits
import Data.Word
import Foreign.C.String
import Foreign.C.Types
import Foreign.Ptr
import Foreign.Storable

#include <BearLibTerminal.h>

--------------------------------------------------------------------------------
-- TYPES

data CodeType = Event | State

-- | The type paramter captures BLT's nuanced API around input and state:
-- * Some 'Code's are only ever events returned by 'read'. Those constants are indexed by 'Event'.
-- * Some 'Code's are only ever to query with 'state'. Those constants are indexed by 'State'.
-- * Some 'Code's may be used as both (e.g. keypress events or key state), and their parameter is left free.
newtype Code (ty :: CodeType) = Code CInt deriving newtype (Eq, Ord, Show, FiniteBits, Bits, Storable)

-- Keyboard scancodes for events/states
#{enum Code ty, Code, TK_A, TK_B, TK_C, TK_D, TK_E, TK_F, TK_G, TK_H, TK_I, TK_J}
#{enum Code ty, Code, TK_K, TK_L, TK_M, TK_N, TK_O, TK_P, TK_Q, TK_R, TK_S, TK_T}
#{enum Code ty, Code, TK_U, TK_V, TK_W, TK_X, TK_Y, TK_Z, TK_0, TK_1, TK_2, TK_3}
#{enum Code ty, Code, TK_4, TK_5, TK_6, TK_7, TK_8, TK_9, TK_RETURN, TK_ENTER}
#{enum Code ty, Code, TK_ESCAPE, TK_BACKSPACE, TK_TAB, TK_SPACE, TK_MINUS, TK_EQUALS}
#{enum Code ty, Code, TK_LBRACKET, TK_RBRACKET, TK_BACKSLASH, TK_SEMICOLON, TK_APOSTROPHE}
#{enum Code ty, Code, TK_GRAVE, TK_COMMA, TK_PERIOD, TK_SLASH, TK_F1, TK_F2, TK_F3}
#{enum Code ty, Code, TK_F4, TK_F5, TK_F6, TK_F7, TK_F8, TK_F9, TK_F10, TK_F11, TK_F12}
#{enum Code ty, Code, TK_PAUSE, TK_INSERT, TK_HOME, TK_PAGEUP, TK_DELETE, TK_END}
#{enum Code ty, Code, TK_PAGEDOWN, TK_RIGHT, TK_LEFT, TK_DOWN, TK_UP, TK_KP_DIVIDE}
#{enum Code ty, Code, TK_KP_MULTIPLY, TK_KP_MINUS, TK_KP_PLUS, TK_KP_ENTER}
#{enum Code ty, Code, TK_KP_0, TK_KP_1, TK_KP_2, TK_KP_3, TK_KP_4, TK_KP_5, TK_KP_6, TK_KP_7}
#{enum Code ty, Code, TK_KP_8, TK_KP_9, TK_KP_PERIOD, TK_SHIFT, TK_CONTROL, TK_ALT}
-- Mouse events/states
#{enum Code ty, Code, TK_MOUSE_LEFT, TK_MOUSE_RIGHT, TK_MOUSE_MIDDLE, TK_MOUSE_X1, TK_MOUSE_X2}
#{enum Code Event, Code, TK_MOUSE_MOVE, TK_MOUSE_SCROLL}
#{enum Code State, Code,  TK_MOUSE_WHEEL, TK_MOUSE_X, TK_MOUSE_Y}
#{enum Code State, Code, TK_MOUSE_PIXEL_X, TK_MOUSE_PIXEL_Y, TK_MOUSE_CLICKS}

-- If key was released instead of pressed, it's code will be OR'ed with TK_KEY_RELEASED:
-- a) pressed 'A': 0x04
-- b) released 'A': 0x04|VK_KEY_RELEASED = 0x104
#{enum Code ty, Code, TK_KEY_RELEASED}

-- Virtual key-codes for internal terminal states/variables.
-- These can be accessed via terminal_state function.
#{enum Code State, Code, TK_WIDTH, TK_HEIGHT, TK_CELL_WIDTH, TK_CELL_HEIGHT}
#{enum Code State, Code, TK_COLOR, TK_BKCOLOR, TK_LAYER, TK_COMPOSITION}
#{enum Code State, Code, TK_CHAR, TK_WCHAR, TK_EVENT, TK_FULLSCREEN}

-- Other events
#{enum Code Event, Code, TK_CLOSE, TK_RESIZED}

-- Input result codes for terminal_read function.
#{enum Code Event, Code, TK_INPUT_NONE, TK_INPUT_CANCELLED}

newtype Alignment = Alignment CInt deriving newtype (Eq, Ord, Show, FiniteBits, Bits, Storable)
#{enum Alignment, Alignment, TK_ALIGN_DEFAULT, TK_ALIGN_LEFT, TK_ALIGN_RIGHT, TK_ALIGN_CENTER}
#{enum Alignment, Alignment, TK_ALIGN_TOP, TK_ALIGN_BOTTOM, TK_ALIGN_MIDDLE}

data Dimensions_t = Dimensions_t
  { width :: CInt
  , height :: CInt
  } deriving (Eq, Show)

instance Storable Dimensions_t where
  alignment _ = #{alignment dimensions_t}
  sizeOf _ = #{size dimensions_t}

  peek p = do
    width <- #{peek dimensions_t, width} p
    height <- #{peek dimensions_t, height} p
    pure Dimensions_t{..}

  poke p Dimensions_t{..} = do
    #{poke dimensions_t, width} p width
    #{poke dimensions_t, height} p height


-- Terminal uses unsigned 32-bit value for color representation in ARGB order (0xAARRGGBB), e. g.
-- a) solid red is 0xFFFF0000
-- b) half-transparent green is 0x8000FF00
newtype Color_t = Color_t CUInt deriving newtype (Eq, Ord, Show, Storable)

newtype Layer = Layer CInt deriving newtype (Eq, Ord, Show, Storable, Num, Integral, Real, Enum, Bounded)

newtype Mode = Mode CInt deriving newtype (Eq, Ord, Show, Num, Storable)

#{enum Mode, Mode, TK_OFF, TK_ON}

--------------------------------------------------------------------------------
-- | TERMINAL_API
foreign import ccall safe "BearLibTerminal.h terminal_open" open
  :: IO CInt

foreign import ccall safe "BearLibTerminal.h terminal_close" close
  :: IO ()

{-
> strings can be encoded using at least three character sizes:
> one (UTF-8 or ANSI), two (UTF-16) and four (UTF-32) bytes.
> Therefore for functions taking string arguments the dynamic-link library
> provides three separate calls. The C/C++ header file hides this fact by
> providing functions for more usual char and wchar_t string types,
> e. g. print and wprint. For some functions there are also variants with
> printf-like formatting, e. g. printf and wprintf. On the other hand, C#
> only use UTF-16 strings and formatting support is usually done by overloading
> the same function. So for C# there is only one printing function while C/C++
> gets four.
-}

foreign import ccall safe "BearLibTerminal.h terminal_set8" set8
  :: Ptr Int8 -- ^ UTF-8 string
  -> IO CInt

foreign import ccall safe "BearLibTerminal.h terminal_set16" set16
  :: Ptr Int16 -- ^ UTF-16 string
  -> IO CInt

foreign import ccall safe "BearLibTerminal.h terminal_set32" set32
  :: Ptr Int32 -- ^ UTF-32 string
  -> IO CInt

foreign import ccall safe "BearLibTerminal.h terminal_refresh" refresh
  :: IO ()

foreign import ccall safe "BearLibTerminal.h terminal_clear" clear
  :: IO ()

foreign import ccall safe "BearLibTerminal.h terminal_clear_area" clear_area
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> CInt -- ^ w
  -> CInt -- ^ h
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_crop" crop
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> CInt -- ^ w
  -> CInt -- ^ h
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_layer" layer
  :: Layer
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_color" color
  :: Color_t
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_bkcolor" bkcolor
  :: Color_t
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_composition" composition
  :: Mode
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_font8" font8
  :: Ptr Int8
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_font16" font16
  :: Ptr Int16
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_font32" font32
  :: Ptr Int32
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_put" put
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> CInt -- ^ code (must be Unicode code point)
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_put_ext" put_ext
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> CInt -- ^ dx
  -> CInt -- ^ dy
  -> CInt -- ^ code (must be Unicode code point)
  -> Ptr Color_t -- ^ corners - an array of FOUR colors
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_pick" pick
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> CInt -- ^ index
  -> IO CInt

foreign import ccall safe "BearLibTerminal.h terminal_pick_color" pick_color
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> CInt -- ^ index
  -> IO Color_t

foreign import ccall safe "BearLibTerminal.h terminal_pick_bkcolor" pick_bkcolor
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> IO Color_t

foreign import ccall safe "terminal_print_ext8" print_ext8
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> Alignment -- ^ align
  -> Ptr CChar -- ^ s (UTF-8 string)
  -> Ptr CInt -- ^ w_out
  -> Ptr CInt -- ^ h out
  -> IO ()

foreign import ccall safe "terminal_print_ext16" print_ext16
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> Alignment -- ^ align
  -> Ptr Int16 -- ^ s (UTF-16 string)
  -> Ptr CInt -- ^ w_out
  -> Ptr CInt -- ^ h out
  -> IO ()

foreign import ccall safe "terminal_print_ext32" print_ext32
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> Alignment -- ^ align
  -> Ptr Int32 -- ^ s (UTF-32 string)
  -> Ptr CInt -- ^ w_out
  -> Ptr CInt -- ^ h out
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_measure_ext8" measure_ext8
  :: CInt -- ^ w
  -> CInt -- ^ h
  -> Ptr CChar -- ^ s (UTF-8 string)
  -> Ptr CInt -- ^ w_out
  -> Ptr CInt -- ^ h_out
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_measure_ext16" measure_ext16
  :: CInt -- ^ w
  -> CInt -- ^ h
  -> Ptr Int8 -- ^ s (UTF-8 string)
  -> Ptr CInt -- ^ w_out
  -> Ptr CInt -- ^ h_out
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_measure_ext32" measure_ext32
  :: CInt -- ^ w
  -> CInt -- ^ h
  -> Ptr Int32 -- ^ s (UTF-8 string)
  -> Ptr CInt -- ^ w_out
  -> Ptr CInt -- ^ h_out
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_has_input" has_input
  :: IO CInt

foreign import ccall safe "BearLibTerminal.h terminal_state" state
  :: Code State
  -> IO CInt

foreign import ccall safe "BearLibTerminal.h terminal_read" read
  :: IO (Code Event)

foreign import ccall safe "BearLibTerminal.h terminal_read_str8" read_str8
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> Ptr CChar -- ^ buffer
  -> CInt -- ^ max
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_read_str16" read_str16
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> Ptr Int16 -- ^ buffer
  -> CInt -- ^ max
  -> IO CInt

foreign import ccall safe "BearLibTerminal.h terminal_read_str32" read_str32
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> Ptr Int32 -- ^ buffer
  -> CInt -- ^ max
  -> IO CInt

foreign import ccall safe "BearLibTerminal.h terminal_peek" peek
  :: IO (Code Event)

foreign import ccall safe "BearLibTerminal.h terminal_delay" delay
  :: CInt -- ^ period
  -> IO ()

foreign import ccall safe "BearLibTerminal.h terminal_get8" get8
  :: Ptr CChar -- ^ key
  -> Ptr CChar -- ^ default
  -> Ptr CChar

foreign import ccall safe "BearLibTerminal.h terminal_get16" get16
  :: Ptr Int16 -- ^ key
  -> Ptr Int16 -- ^ default
  -> Ptr Int16

foreign import ccall safe "BearLibTerminal.h terminal_get32" get32
  :: Ptr Int32 -- ^ key
  -> Ptr Int32 -- ^ default
  -> Ptr Int32

foreign import ccall safe "BearLibTerminal.h color_from_name8" color_from_name8
  :: Ptr CChar
  -> Color_t

foreign import ccall safe "BearLibTerminal.h color_from_name16" color_from_name16
  :: Ptr Int16
  -> Color_t

foreign import ccall safe "BearLibTerminal.h color_from_name32" color_from_name32
  :: Ptr Int32
  -> Color_t

--------------------------------------------------------------------------------
-- TERMINAL_INLINE SHIMS
#def int ffi_terminal_set(const char* s) { return terminal_set(s); }
foreign import ccall safe "ffi_terminal_set" set
  :: CString
  -> IO CInt

#def void ffi_terminal_print(int x, int y, const char* s, dimensions_t* out) { *out = terminal_print(x, y, s); return; }
foreign import ccall safe "ffi_terminal_print" print
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> CString -- ^ s
  -> Ptr Dimensions_t
  -> IO ()

#def void ffi_terminal_print_ext(int x, int y, int w, int h, int a, const char* s, dimensions_t* out) { *out = terminal_print_ext(x, y, w, h, a, s); return; }
foreign import ccall safe "ffi_terminal_print_ext" print_ext
  :: CInt -- ^ x
  -> CInt -- ^ y
  -> CInt -- ^ width
  -> CInt -- ^ height
  -> Alignment -- ^ align
  -> CString -- ^ s
  -> Ptr Dimensions_t -- ^ out
  -> IO ()

#def void ffi_terminal_measure(const char* s, dimensions_t* out) { *out = terminal_measure(s); return; }
foreign import ccall safe "ffi_terminal_measure" measure
  :: CString -- ^ s
  -> Ptr Dimensions_t -- ^ out
  -> IO ()

#def void ffi_terminal_measure_ext(int w, int h, const char* s, dimensions_t* out) { *out = terminal_measure_ext(w, h, s); return; }
foreign import ccall safe "ffi_terminal_measure_ext" measure_ext
  :: CInt -- ^ width
  -> CInt -- ^ height
  -> CString -- ^ s
  -> Ptr Dimensions_t -- ^ out

#def color_t ffi_color_from_argb(uint8_t a, uint8_t r, uint8_t g, uint8_t b) { return color_from_argb(a, r, g, b); }
foreign import ccall unsafe "ffi_color_from_argb" color_from_argb
  :: Word8 -- ^ a
  -> Word8 -- ^ r
  -> Word8 -- ^ g
  -> Word8 -- ^ b
  -> Color_t

#def int ffi_terminal_check(int code) { return terminal_check(code); }
foreign import ccall safe "ffi_terminal_check" check
  :: Code State
  -> CInt -- ^ 0 or 1
