module BearLibTerminal.Simple where

import Control.Monad (unless)
import Control.Exception (bracket_)

import qualified BearLibTerminal.C as C
import qualified BearLibTerminal.Config as Config


-- TODO: Move all types to .C.Types.* for an easier re-export & haddocking

{-
 - Config
 - withTerminal :: Configs -> IO a -> IO a
 - clear / clearArea
 - refresh
 - crop
 - layer
 - color / bkcolor
 - composition
 - put / putExt
 - print
 - measure
 - state
 - read (event)
 - readStr
 - peek
 - set
 - get
 - color_from_argb/name (higher level?)
--}

withTerminal :: Config.Params -> IO a -> IO a
withTerminal cfg ioa = do
  setSuccess <- Config.set cfg
  unless setSuccess $ error "Config.set FAILED"
  bracket_ C.open C.close ioa

-- TODO:
-- - Its own module
-- - IsString instance (-> Lit)
-- - Num instance (-> Code so we can just use hex)
-- - Combinators (lines, words, etc)
-- - Operators
data PrintString =
    PrintLit String
  | PrintMany [PrintString]
  | PrintColor PrintString -- TODO: Color type
  | PrintBkColor PrintString -- TODO: Color type
  | PrintCode -- TODO: What to pass here
  | PrintCombined Char Char -- TODO: Or smth
  | PrintOffset Int Int PrintString
  | PrintFont String PrintString

-- TODO: Also allow people to use bare BLT format strings
