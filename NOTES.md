TODO: Translate this to org mode

### Working around `static inline`

BearLibTerminal marks much of its documented C API as `static inline`. This works fine when you `#include` the library in C (I think), but when Haskell links to it, those functions are not accessible (due to `static` I believe.)

My workaround is to use `hsc2hs`'s `#def` to define a one-line wrapper that calls the function.

This trick is also useful when C functions return structs (which GHC's FFI does not support.) You can quickly `#def` an API that passes in an `out` pointer.

### `hsc` <> `ghci`

* Sometimes errors don't appear until I close `ghci`
* Reloading doesn't work at all
* Need to `:set -fobject-code` to run examples in `ghci`

### My FFI Process

* Bind as literally as possible to the C API.
  * Inline copied documentation
  * `newtype` aggressively. Phantom types can also help type C APIs. Both of these play nice with `hsc`'s `const` and `enum`
  * Can use `#def` to work around FFI limitations for struct returns
* ???

### References
* https://jrgraphix.net/r/Unicode/2500-257F
